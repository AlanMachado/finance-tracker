class Rename < ActiveRecord::Migration
  def change
    rename_column :stocks, :las_price, :last_price
  end
end
